import tensorflow as tf

# map to the expected input to TFBertForSequenceClassification, see here 
def map_example_to_dict(input_ids, attention_masks, token_type_ids, label):
    return {
            "input_ids": input_ids,
            "token_type_ids": token_type_ids,
            "attention_mask": attention_masks,
    }, label


def encode_examples(ds, labels, limit=-1):
    # prepare list, so that we can build up final TensorFlow dataset from slices.
    return tf.data.Dataset.from_tensor_slices((ds['input_ids'], ds['token_type_ids'], ds['attention_mask'], labels)).map(map_example_to_dict)
