import torch

model_name = 'bert'
bert_model_name = 'bert-base-cased' #distilbert-base-cased
num_output_layers = 2
train_transformer = True
max_sequence_len = 256
batch_size = 8
train_adversarial = False
train_arct = False
train_reasoning = True
train_race = False
epochs = 1

# If there's a GPU available...
if torch.cuda.is_available():    

    # Tell PyTorch to use the GPU.    
    device = torch.device("cuda")

    print('There are %d GPU(s) available.' % torch.cuda.device_count())

    print('We will use the GPU:', torch.cuda.get_device_name(0))

# If not...
else:
    print('No GPU available, using the CPU instead.')
    device = torch.device("cpu")
