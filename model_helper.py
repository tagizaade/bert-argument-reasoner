from transformers import AutoModel, AutoTokenizer, AlbertForMultipleChoice, AlbertForSequenceClassification, DistilBertForSequenceClassification, DistilBertForMultipleChoice, BertForMultipleChoice, BertForSequenceClassification

def get_tokenizer(args):
    return AutoTokenizer.from_pretrained(args.transformer_path, do_lower_case=True)


def get_model(args):
    if args.transformer_type == 'albert':
        if args.task_type == 'multiple_choice':
            model = AlbertForMultipleChoice.from_pretrained(args.transformer_path)
        elif args.task_type == 'single_choice':
            model = AlbertForSequenceClassification.from_pretrained(args.transformer_path)
    elif args.transformer_type == 'distilbert':
        if args.task_type == 'multiple_choice':
            model = DistilBertForMultipleChoice.from_pretrained(args.transformer_path)
        elif args.task_type == 'single_choice':
            model = DistilBertForSequenceClassification.from_pretrained(args.transformer_path)
    elif args.transformer_type == 'bert':
        if args.task_type == 'multiple_choice':
            model = BertForMultipleChoice.from_pretrained(args.transformer_path)
        elif args.task_type == 'single_choice':
            model = BertForSequenceClassification.from_pretrained(args.transformer_path)
    else:
        print('Not Configured')
        exit(0)
    return model
