import glob
import json
import logging

import torch
from torch.utils.data import TensorDataset, random_split, ConcatDataset, DataLoader, RandomSampler, SequentialSampler
import numpy as np
import pandas as pd

logger = logging.getLogger(__name__)

class RaceExample:
    """A single training/test example for the RACE dataset."""
    '''
    For RACE dataset:
    race_id: data id
    context_sentence: article
    start_ending: question
    ending_0/1/2/3: option_0/1/2/3
    label: true answer
    '''
    def __init__(self,
                 race_id,
                 context_sentence,
                 start_ending,
                 ending_0,
                 ending_1,
                 ending_2,
                 ending_3,
                 label = None):
        self.race_id = race_id
        self.context_sentence = context_sentence
        self.start_ending = start_ending
        self.endings = [
            ending_0,
            ending_1,
            ending_2,
            ending_3,
        ]
        self.label = label
    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        l = [
            f"id: {self.race_id}",
            f"article: {self.context_sentence}",
            f"question: {self.start_ending}",
            f"option_0: {self.endings[0]}",
            f"option_1: {self.endings[1]}",
            f"option_2: {self.endings[2]}",
            f"option_3: {self.endings[3]}",
        ]

        if self.label is not None:
            l.append(f"label: {self.label}")

        return ", ".join(l)

class InputFeatures:
    def __init__(self,
                 example_id,
                 choices_features,
                 label

    ):
        self.example_id = example_id
        self.choices_features = [
            {
                'input_ids': input_ids,
                'input_mask': input_mask,
                'segment_ids': segment_ids
            }
            for _, input_ids, input_mask, segment_ids in choices_features
        ]
        self.label = label

def read_race_examples(paths):
    examples = []
    for path in paths:
        filenames = glob.glob(path+"/*txt")
        for filename in filenames:
            with open(filename, 'r', encoding='utf-8') as fpr:
                data_raw = json.load(fpr)
                article = data_raw['article']
                ## for each qn
                for i in range(len(data_raw['answers'])):
                    truth = ord(data_raw['answers'][i]) - ord('A')
                    question = data_raw['questions'][i]
                    options = data_raw['options'][i]
                    examples.append(
                        RaceExample(
                            race_id = filename+'-'+str(i),
                            context_sentence = article,
                            start_ending = question,

                            ending_0 = options[0],
                            ending_1 = options[1],
                            ending_2 = options[2],
                            ending_3 = options[3],
                            label = truth))
                
    return examples

def convert_examples_to_features(examples, tokenizer, max_seq_length, is_training):
    """Loads a data file into a list of `InputBatch`s."""

    # RACE is a multiple choice task. To perform this task using Bert,
    # we will use the formatting proposed in "Improving Language
    # Understanding by Generative Pre-Training" and suggested by
    # @jacobdevlin-google in this issue
    # https://github.com/google-research/bert/issues/38.
    #
    # The input will be like:
    # [CLS] Article [SEP] Question + Option [SEP]
    # for each option 
    # 
    # The model will output a single value for each input. To get the
    # final decision of the model, we will run a softmax over these 4
    # outputs.
    features = []
    for example_index, example in enumerate(examples):
        context_tokens = tokenizer.tokenize(example.context_sentence)
        start_ending_tokens = tokenizer.tokenize(example.start_ending)

        choices_features = []
        for ending_index, ending in enumerate(example.endings):
            # We create a copy of the context tokens in order to be
            # able to shrink it according to ending_tokens
            context_tokens_choice = context_tokens[:]
            ending_tokens = start_ending_tokens + tokenizer.tokenize(ending)
            # Modifies `context_tokens_choice` and `ending_tokens` in
            # place so that the total length is less than the
            # specified length.  Account for [CLS], [SEP], [SEP] with
            # "- 3"
            _truncate_seq_pair(context_tokens_choice, ending_tokens, max_seq_length - 3)

            tokens = ["[CLS]"] + context_tokens_choice + ["[SEP]"] + ending_tokens + ["[SEP]"]
            segment_ids = [0] * (len(context_tokens_choice) + 2) + [1] * (len(ending_tokens) + 1)

            input_ids = tokenizer.convert_tokens_to_ids(tokens)
            input_mask = [1] * len(input_ids)

            # Zero-pad up to the sequence length.
            padding = [0] * (max_seq_length - len(input_ids))
            input_ids += padding
            input_mask += padding
            segment_ids += padding

            assert len(input_ids) == max_seq_length
            assert len(input_mask) == max_seq_length
            assert len(segment_ids) == max_seq_length

            choices_features.append((tokens, input_ids, input_mask, segment_ids))

        label = example.label
        ## display some example
        if example_index < 1:
            logger.info("*** Example ***")
            logger.info(f"race_id: {example.race_id}")
            for choice_idx, (tokens, input_ids, input_mask, segment_ids) in enumerate(choices_features):
                logger.info(f"choice: {choice_idx}")
                logger.info(f"tokens: {' '.join(tokens)}")
                logger.info(f"input_ids: {' '.join(map(str, input_ids))}")
                logger.info(f"input_mask: {' '.join(map(str, input_mask))}")
                logger.info(f"segment_ids: {' '.join(map(str, segment_ids))}")
            if is_training:
                logger.info(f"label: {label}")

        features.append(
            InputFeatures(
                example_id = example.race_id,
                choices_features = choices_features,
                label = label
            )
        )

    return features

def _truncate_seq_pair(tokens_a, tokens_b, max_length):
    """Truncates a sequence pair in place to the maximum length."""

    # This is a simple heuristic which will always truncate the longer sequence
    # one token at a time. This makes more sense than truncating an equal percent
    # of tokens from each, since if one sequence is very short then each token
    # that's truncated likely contains more information than a longer sequence.
    while True:
        total_length = len(tokens_a) + len(tokens_b)
        if total_length <= max_length:
            break
        if len(tokens_a) > len(tokens_b):
            tokens_a.pop()
        else:
            tokens_b.pop()
def select_field(features, field):
    return [
        [
            choice[field]
            for choice in feature.choices_features
        ]
        for feature in features
    ]


def get_labels_tensors(df, model_type):
    labels = df['correctLabelW0orW1'].values
    if model_type == 'multiple_choice':
        logger.debug("calculating labels shape " + str(labels.shape))
        return torch.tensor(labels, dtype=torch.long)
    targets1 = np.zeros(len(labels), dtype=int)
    targets1[labels == 0] = 1
    targets2 = np.zeros(len(labels), dtype=int)
    targets2[labels == 1] = 1
    a = np.append(targets1, targets2, axis=0)
    return torch.tensor(a, dtype=torch.long)

def get_inputs_tensors(df, model_type, tokenizer, max_seq_len):
    t_input_ids = []
    t_attentions_masks = []
    t_segment_ids = []
    for _, row in df.iterrows():
        c_input_ids = []
        c_attentions_masks = []
        c_segment_ids = []
        reason = row['reason']
        claim = row['claim']
        warrant0 = row['warrant0']
        warrant1 = row['warrant1']
        start_tokens = tokenizer.tokenize(reason + claim)
        for warrant in [warrant0, warrant1]:
            c_start_tokens = start_tokens[:]
            ending_tokens = tokenizer.tokenize(warrant)
            _truncate_seq_pair(c_start_tokens, ending_tokens, max_seq_len - 3)
            tokens = ["[CLS]"] + c_start_tokens + ["[SEP]"] + ending_tokens + ["[SEP]"]
            segment_ids = [0] * (len(c_start_tokens) + 2) + [1] * (len(ending_tokens) + 1)
            input_ids = tokenizer.convert_tokens_to_ids(tokens)
            input_mask = [1] * len(input_ids)
            # Zero-pad up to the sequence length.
            padding = [0] * (max_seq_len - len(input_ids))
            
            input_ids += padding
            input_mask += padding
            segment_ids += padding
            
            
            assert len(input_ids) == max_seq_len
            assert len(input_mask) == max_seq_len
            assert len(segment_ids) == max_seq_len
            
            c_input_ids.append(input_ids)
            c_attentions_masks.append(input_mask)
            c_segment_ids.append(segment_ids)
            
            
            
        if model_type == 'multiple_choice':
            t_input_ids.append(c_input_ids)
            t_attentions_masks.append(c_attentions_masks)
            t_segment_ids.append(c_segment_ids)
        else:
            t_input_ids.extend(c_input_ids)
            t_attentions_masks.extend(c_attentions_masks)
            t_segment_ids.extend(c_segment_ids)
    
        
    return torch.tensor(t_input_ids, dtype=torch.long), torch.tensor(t_attentions_masks, dtype=torch.long), torch.tensor(t_segment_ids, dtype=torch.long) 


def read_reasoning_dataset(path, tokenizer, max_seq_len, model_type):
    t_input_ids = []
    t_segment_ids = []
    t_attentions_masks = []
    labels = []
    with open(path) as f:
        for line in f:
            sample = json.loads(line)
            context = sample['context']
            questions = sample['questions']
            added_questions = []
            start_tokens = tokenizer.tokenize(context)            
            for q in questions:
                text = q['text']
                
                if text in added_questions:
                    continue
                added_questions.append(text)
                
                if ' is ' not in text:
                    continue
                    
                if 'not' in text:
                    new_q = text.replace('not', '')
                else:
                    new_q = text.replace('is', 'is not')
                added_questions.append(new_q)
                c_input_ids = []
                c_attentions_masks = []
                c_segment_ids = []
                for qq in [text, new_q]:
                    
                    c_start_tokens = start_tokens[:]
                    ending_tokens = tokenizer.tokenize(qq)
                
                    _truncate_seq_pair(c_start_tokens, ending_tokens, max_seq_len - 3)
                
                    tokens = ["[CLS]"] + c_start_tokens + ["[SEP]"] + ending_tokens + ["[SEP]"]
                
                    segment_ids = [0] * (len(c_start_tokens) + 2) + [1] * (len(ending_tokens) + 1)
                    input_ids = tokenizer.convert_tokens_to_ids(tokens)
                    input_mask = [1] * len(input_ids)
                
                    padding = [0] * (max_seq_len - len(input_ids))
            
                    input_ids += padding
                    input_mask += padding
                    segment_ids += padding
            
            
                    assert len(input_ids) == max_seq_len
                    assert len(input_mask) == max_seq_len
                    assert len(segment_ids) == max_seq_len
            
                    c_input_ids.append(input_ids)
                    c_attentions_masks.append(input_mask)
                    c_segment_ids.append(segment_ids)
                    
                if model_type == 'multiple_choice':
                    t_input_ids.append(c_input_ids)
                    t_attentions_masks.append(c_attentions_masks)
                    t_segment_ids.append(c_segment_ids)
                else:
                    t_input_ids.extend(c_input_ids)
                    t_attentions_masks.extend(c_attentions_masks)
                    t_segment_ids.extend(c_segment_ids)
                
                label = 0 if q['label'] == True else 1
                labels.append(label)
    return torch.tensor(t_input_ids, dtype=torch.long), torch.tensor(t_attentions_masks, dtype=torch.long), torch.tensor(t_segment_ids, dtype=torch.long), torch.tensor(labels, dtype=torch.long)



def get_race_train(tokenizer, max_seq_len):
    race_train_examples = read_race_examples(['data/race/train/middle'])
    race_train_features = convert_examples_to_features(race_train_examples, tokenizer, max_seq_len, True)
    race_train_input_ids = torch.tensor(select_field(race_train_features, 'input_ids'), dtype=torch.long)
    race_train_input_mask = torch.tensor(select_field(race_train_features, 'input_mask'), dtype=torch.long)
    race_train_segment_ids = torch.tensor(select_field(race_train_features, 'segment_ids'), dtype=torch.long)
    race_train_label = torch.tensor([f.label for f in race_train_features], dtype=torch.long)
    return TensorDataset(race_train_input_ids, race_train_input_mask, race_train_segment_ids, race_train_label)

def get_race_dev(tokenizer, max_seq_len):
    race_dev_examples = read_race_examples(['data/race/dev/middle'])
    race_dev_features = convert_examples_to_features(race_dev_examples, tokenizer, max_seq_len, True)
    race_dev_input_ids = torch.tensor(select_field(race_dev_features, 'input_ids'), dtype=torch.long)
    race_dev_input_mask = torch.tensor(select_field(race_dev_features, 'input_mask'), dtype=torch.long)
    race_dev_segment_ids = torch.tensor(select_field(race_dev_features, 'segment_ids'), dtype=torch.long)
    race_dev_label = torch.tensor([f.label for f in race_dev_features], dtype=torch.long)
    return TensorDataset(race_dev_input_ids, race_dev_input_mask, race_dev_segment_ids, race_dev_label)

def get_race_test(tokenizer, max_seq_len):
    race_test_examples = read_race_examples(['data/race/test/middle'])
    race_test_features = convert_examples_to_features(race_test_examples, tokenizer, max_seq_len, True)
    race_test_input_ids = torch.tensor(select_field(race_test_features, 'input_ids'), dtype=torch.long)
    race_test_input_mask = torch.tensor(select_field(race_test_features, 'input_mask'), dtype=torch.long)
    race_test_segment_ids = torch.tensor(select_field(race_test_features, 'segment_ids'), dtype=torch.long)
    race_test_label = torch.tensor([f.label for f in race_test_features], dtype=torch.long)
    return TensorDataset(race_test_input_ids, race_test_input_mask, race_test_segment_ids, race_test_label)


def get_arct_train(model_type, tokenizer, max_seq_len):
    arct_set = pd.read_csv('data/arct/train-full.txt', sep='\t')
    input_ids, attentions_masks, segment_ids = get_inputs_tensors(arct_set, model_type, tokenizer, max_seq_len)
    labels = get_labels_tensors(arct_set, model_type)
    logger.debug(input_ids.shape)
    return TensorDataset(input_ids, attentions_masks, segment_ids, labels)

def get_arct_dev(model_type, tokenizer, max_seq_len):
    arct_set = pd.read_csv('data/arct/dev-full.txt', sep='\t')
    input_ids, attentions_masks, segment_ids = get_inputs_tensors(arct_set, model_type, tokenizer, max_seq_len)
    labels = get_labels_tensors(arct_set, model_type)
    return TensorDataset(input_ids, attentions_masks, segment_ids, labels)

def get_arct_test(model_type, tokenizer, max_seq_len):
    arct_set = pd.read_csv('data/arct/test-full.txt', sep='\t')
    input_ids, attentions_masks, segment_ids = get_inputs_tensors(arct_set, model_type, tokenizer, max_seq_len)
    labels = get_labels_tensors(arct_set, model_type)
    return TensorDataset(input_ids, attentions_masks, segment_ids, labels)



def get_adversarial_train(model_type, tokenizer, max_seq_len):
    adversarial_set = pd.read_csv('data/adversarial/train-negated.csv', sep='\t')
    input_ids, attentions_masks, segment_ids = get_inputs_tensors(adversarial_set, model_type, tokenizer, max_seq_len)
    labels = get_labels_tensors(adversarial_set, model_type)
    return TensorDataset(input_ids, attentions_masks, segment_ids, labels)

def get_adversarial_dev(model_type, tokenizer, max_seq_len):
    adversarial_set = pd.read_csv('data/adversarial/dev-negated.csv', sep='\t')
    input_ids, attentions_masks, segment_ids = get_inputs_tensors(adversarial_set, model_type, tokenizer, max_seq_len)
    labels = get_labels_tensors(adversarial_set, model_type)
    return TensorDataset(input_ids, attentions_masks, segment_ids, labels)

def get_adversarial_test(model_type, tokenizer, max_seq_len):
    adversarial_set = pd.read_csv('data/adversarial/test-negated.csv', sep='\t')
    input_ids, attentions_masks, segment_ids = get_inputs_tensors(adversarial_set, model_type, tokenizer, max_seq_len)
    labels = get_labels_tensors(adversarial_set, model_type)
    return TensorDataset(input_ids, attentions_masks, segment_ids, labels)

                                         
def get_reasoning_train(tokenizer, max_seq_len, model_type):
    input_ids, attention_masks, segment_ids, labels = read_reasoning_dataset('data/reasoning/depth-3ext-NatLang/train.jsonl', tokenizer, max_seq_len, model_type)
    return TensorDataset(input_ids, attention_masks, segment_ids, labels)

def get_reasoning_dev(tokenizer, max_seq_len, model_type):
    input_ids, attention_masks, segment_ids, labels = read_reasoning_dataset('data/reasoning/depth-3ext-NatLang/dev.jsonl', tokenizer, max_seq_len, model_type)
    return TensorDataset(input_ids, attention_masks, segment_ids, labels)
    
def get_reasoning_test(tokenizer, max_seq_len, model_type):
    input_ids, attention_masks, segment_ids, labels = read_reasoning_dataset('data/reasoning/depth-3ext-NatLang/test.jsonl', tokenizer, max_seq_len, model_type)
    return TensorDataset(input_ids, attention_masks, segment_ids, labels)
                                         
def get_train_data_loader(args, tokenizer):
    train_datasets = []

    if args.train_arct:
        train_datasets.append(get_arct_train(args.task_type, tokenizer, args.max_seq_len))

    if args.train_adversarial:
        train_datasets.append(get_adversarial_train(args.task_type, tokenizer, args.max_seq_len))
        
    if args.train_race:
        train_datasets.append(get_race_train(tokenizer, args.max_seq_len))
    
    if args.train_reasoning:
        train_datasets.append(get_reasoning_train(tokenizer, args.max_seq_len, args.task_type))

    train_dataset = ConcatDataset(train_datasets)

    train_dataloader = DataLoader(
            train_dataset,  # The training samples.
            sampler = RandomSampler(train_dataset), # Select batches randomly
            batch_size = args.batch_size # Trains with this batch size.
        )
    return train_dataloader
    

def get_dev_data_loader(args, tokenizer):
    dev_datasets = []

    if args.dev_arct:
        dev_datasets.append(get_arct_dev(args.task_type, tokenizer, args.max_seq_len))

    if args.dev_adversarial:
        dev_datasets.append(get_adversarial_dev(args.task_type, tokenizer, args.max_seq_len))
        
    if args.dev_race:
        dev_datasets.append(get_race_dev(tokenizer, args.max_seq_len))
    
    if args.dev_reasoning:
        dev_datasets.append(get_reasoning_dev(tokenizer, args.max_seq_len, args.task_type))

    dev_dataset = ConcatDataset(dev_datasets)

    dev_dataloader = DataLoader(
            dev_dataset,  # The deving samples.
            sampler = RandomSampler(dev_dataset), # Select batches randomly
            batch_size = args.batch_size # devs with this batch size.
        )
    return dev_dataloader


def get_test_data_loader(args, tokenizer):
    dev_datasets = []

    if args.test_arct:
        dev_datasets.append(get_arct_test(args.task_type, tokenizer, args.max_seq_len))

    if args.test_adversarial:
        dev_datasets.append(get_adversarial_test(args.task_type, tokenizer, args.max_seq_len))
        
    if args.test_race:
        dev_datasets.append(get_race_test(tokenizer, args.max_seq_len))
                                         
    if args.test_reasoning:
        dev_datasets.append(get_reasoning_test(tokenizer, args.max_seq_len, args.task_type))
    
    dev_dataset = ConcatDataset(dev_datasets)

    dev_dataloader = DataLoader(
            dev_dataset,  # The deving samples.
            sampler = RandomSampler(dev_dataset), # Select batches randomly
            batch_size = args.batch_size # devs with this batch size.
        )
    return dev_dataloader


