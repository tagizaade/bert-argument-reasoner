import logging
import time
import datetime
import random

import torch

from my_arg_parser import get_arguments
import model_helper
import data_helper
from transformers import AdamW
from transformers import get_linear_schedule_with_warmup
import numpy as np



logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(name)s -   %(message)s',
                    datefmt = '%m/%d/%Y %H:%M:%S',
                    level = logging.INFO)
logger = logging.getLogger(__name__)


def format_time(elapsed):
    '''
    Takes a time in seconds and returns a string hh:mm:ss
    '''
    # Round to the nearest second.
    elapsed_rounded = int(round((elapsed)))
    
    # Format as hh:mm:ss
    return str(datetime.timedelta(seconds=elapsed_rounded))

def flat_accuracy(preds, labels):
    pred_flat = np.argmax(preds, axis=1).flatten()
    labels_flat = labels.flatten()
    sorat = np.sum(pred_flat == labels_flat) 
    makharj = len(labels_flat)
    return sorat/makharj

args = get_arguments()

# Check if GPU available
if torch.cuda.is_available():    
    device = torch.device("cuda")
    logger.info('There are %d GPU(s) available.' % torch.cuda.device_count())
    logger.info('We will use the GPU: ' + str(torch.cuda.get_device_name(0)))
else:
    logger.warn('No GPU available, using the CPU instead.')
    device = torch.device("cpu")


random.seed(args.seed_val)
np.random.seed(args.seed_val)
torch.manual_seed(args.seed_val)
torch.cuda.manual_seed_all(args.seed_val)

tokenizer = model_helper.get_tokenizer(args)

if args.train:
    if args.load_from_checkpoint:
        model = torch.load(args.checkpoint_path)
    else:
        model = model_helper.get_model(args)

    model.to(device)
    if args.freeze_transfromer:
        for name, param in model.named_parameters():
            if 'classifier' not in name: # classifier layer
                param.requires_grad = False
    
    train_dataloader = data_helper.get_train_data_loader(args, tokenizer)
    dev_dataloader = data_helper.get_dev_data_loader(args, tokenizer)

    optimizer = AdamW(model.parameters(), lr = 5e-5, eps = 1e-8)

    total_steps = len(train_dataloader) * args.epochs

    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps = 0, num_training_steps = total_steps)

    random.seed(args.seed_val)
    np.random.seed(args.seed_val)
    torch.manual_seed(args.seed_val)
    torch.cuda.manual_seed_all(args.seed_val)
    best_acc = -1000
    best_loss = 1000
    training_stats = []
    total_t0 = time.time()
    for epoch_i in range(0, args.epochs):
        
        print("")
        print('======== Epoch {:} / {:} ========'.format(epoch_i + 1, args.epochs))
        print('Training...')

        # Measure how long the training epoch takes.
        t0 = time.time()

        total_train_loss = 0

        # change behaviour of dropout and batch normalization
        model.train()

        for step, batch in enumerate(train_dataloader):

            # Progress update every 40 batches.
            if step % 40 == 0 and not step == 0:
                # Calculate elapsed time in minutes.
                elapsed = format_time(time.time() - t0)

                # Report progress.
                print('  Batch {:>5,}  of  {:>5,}.    Elapsed: {:}.'.format(step, len(train_dataloader), elapsed))
            
            # `batch` contains three pytorch tensors:
            #   [0]: input ids 
            #   [1]: attention masks
            #   [2]: labels 
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            b_seg_ids = None
            if len(batch) > 3:
                b_seg_ids = batch[2].to(device)
            b_labels = batch[-1].to(device)
            logger.debug(b_labels.shape)

            # clear any previously calculated gradients before performing a backward pass
            model.zero_grad()        

            # Perform a forward pass (evaluate the model on this training batch).
            if args.transformer_type == 'distilbert':
                loss, logits = model(input_ids=b_input_ids, attention_mask=b_input_mask, labels=b_labels)
            else:
                loss, logits = model(input_ids=b_input_ids, attention_mask=b_input_mask, token_type_ids=b_seg_ids, labels=b_labels)

            # Accumulate the training loss over all of the batches so that we can
            # calculate the average loss at the end.
            total_train_loss += loss.item()

            # Perform a backward pass to calculate the gradients.
            loss.backward()

            # Clip the norm of the gradients to 1.0.
            torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

            # Update parameters and take a step using the computed gradient.
            optimizer.step()

            # Update the learning rate.
            scheduler.step()

        # Calculate the average loss over all of the batches.
        avg_train_loss = total_train_loss / len(train_dataloader)            

        # Measure how long this epoch took.
        training_time = format_time(time.time() - t0)

        print("")
        print("  Average training loss: {0:.2f}".format(avg_train_loss))
        print("  Training epcoh took: {:}".format(training_time))

        print("")
        print("Running Validation...")

        t0 = time.time()

        # Put the model in evaluation mode--the dropout layers behave differently
        # during evaluation.
        model.eval()

        # Tracking variables 
        total_eval_accuracy = 0
        total_eval_loss = 0
        nb_eval_steps = 0

        # Evaluate data for one epoch
        for batch in dev_dataloader:

            # `batch` contains three pytorch tensors:
            #   [0]: input ids 
            #   [1]: attention masks
            #   [2]: labels 
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            if len(batch) > 3:
                b_seg_ids = batch[2].to(device)
            b_labels = batch[-1].to(device)

            # Tell pytorch not to bother with constructing the compute graph during
            # the forward pass, since this is only needed for backprop (training).
            with torch.no_grad():
                if args.transformer_type == 'distilbert':
                    loss, logits = model(input_ids=b_input_ids, attention_mask=b_input_mask, labels=b_labels)
                else:
                    loss, logits = model(input_ids=b_input_ids, attention_mask=b_input_mask, token_type_ids=b_seg_ids, labels=b_labels)

            # Accumulate the validation loss.
            total_eval_loss += loss.item()

            # Move logits and labels to CPU
            logits = logits.detach().cpu().numpy()
            label_ids = b_labels.to('cpu').numpy()

            # Calculate the accuracy for this batch of test sentences, and
            # accumulate it over all batches.
            total_eval_accuracy += flat_accuracy(logits, label_ids)


        # Report the final accuracy for this validation run.
        avg_val_accuracy = total_eval_accuracy / len(dev_dataloader)
        print("  Accuracy: {0:.4f}".format(avg_val_accuracy))
        # if avg_val_accuracy > best_acc:
        #     best_acc = avg_val_accuracy
        #     torch.save(model, args.checkpoint_path)

        # Calculate the average loss over all of the batches.
        avg_val_loss = total_eval_loss / len(dev_dataloader)
        if avg_val_loss <= best_loss:
            best_loss = avg_val_loss
            torch.save(model, args.checkpoint_path)

        # Measure how long the validation run took.
        validation_time = format_time(time.time() - t0)

        print("  Validation Loss: {0:.4f}".format(avg_val_loss))
        print("  Validation took: {:}".format(validation_time))

        # Record all statistics from this epoch.
        training_stats.append(
            {
                'epoch': epoch_i + 1,
                'Training Loss': avg_train_loss,
                'Valid. Loss': avg_val_loss,
                'Valid. Accur.': avg_val_accuracy,
                'Training Time': training_time,
                'Validation Time': validation_time
            }
        )

    print("")
    print("Training complete!")

    print("Total training took {:} (h:mm:ss)".format(format_time(time.time()-total_t0)))

    
if args.test:
    if args.load_from_checkpoint:
        model = torch.load(args.checkpoint_path)
    else:
        model = model_helper.get_model(args)
    model.to(device)
    model.eval()
    test_dataloader = data_helper.get_test_data_loader(args, tokenizer)
    t0 = time.time()
    total_test_loss = 0
    total_test_accuracy = 0
    for batch in test_dataloader:
        b_input_ids = batch[0].to(device)
        b_input_mask = batch[1].to(device)
        b_seg_ids = None
        if len(batch) > 3:
            b_seg_ids = batch[2].to(device)
        b_labels = batch[-1].to(device)

        with torch.no_grad():
            if args.transformer_type == 'distilbert':
                loss, logits = model(input_ids=b_input_ids, attention_mask=b_input_mask, labels=b_labels)
            else:
                loss, logits = model(input_ids=b_input_ids, attention_mask=b_input_mask, token_type_ids=b_seg_ids, labels=b_labels)
        total_test_loss += loss.item()
        logits = logits.detach().cpu().numpy()
        label_ids = b_labels.to('cpu').numpy()
        
        total_test_accuracy += flat_accuracy(logits, label_ids)

    avg_test_accuracy = total_test_accuracy / len(test_dataloader)
    print("Test  accuracy: {0:.4f}".format(avg_test_accuracy))
  
    avg_test_loss = total_test_loss / len(test_dataloader)
    print("  Test Loss: {0:.4f}".format(avg_test_loss))

    test_time = format_time(time.time() - t0)
    print("  Test took: {:}".format(test_time))
    

    
